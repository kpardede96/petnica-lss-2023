# petnica LSS 2023

Bunch of LSS stuffs for Petnica Astrophysics Summer School 2023 [[link](http://psi.petnica.rs/2023/description.php)] 

> For comments and questions please contact: kfspardede (at) gmail (dot) com


_**References:**_

**LSS 1**
1. Mainly from Daniel Baumann "Cosmology" book: [here](https://www.amazon.com/Cosmology-Daniel-Baumann/dp/1108838073). The TASI version is very nice too: [here](http://physics.bu.edu/~schmaltz/PY555/baumann_notes.pdf) for example.
2. Short "back of the envelope calculation" of the "Jeans scale" from Mehrdad Mirbabayi ICTP GR-2 course: [here](http://users.ictp.it/~mirbabayi/gr_files/notes_grII.pdf) (check the "Structure Formation" section).
3. Nice derivation of continuity equation and Euler equation is presented in Box 10.4 of "Relativity, Gravitation and Cosmology" book by Ta-Pei Chang: [here](https://www.tapeicheng.com/books). In general also a very nice book on GR and Cosmology!

**DM**
1. For a nice discussion on evidence of dark matter from flat rotation curves, check Mariangle Lisanti TASI lectures on "Dark Matter Physics": [here](https://arxiv.org/abs/1603.03797)
2. Good general dark matter discussion can be found in Barbara Ryden "Cosmology" book: [here](http://carina.fcaglp.unlp.edu.ar/extragalactica/Bibliografia/Ryden_IntroCosmo.pdf)

**LSS 2**
1. The classics are "LSS and Cosmo PT": [here](https://arxiv.org/abs/astro-ph/0112551) and "Large-Scale Galaxy Bias": [here](https://arxiv.org/abs/1611.09787)
2. "Modern Cosmology" edition 2, by Scott Dodelson & Fabian Schmidt also contain a section on perturbation theory: [here](https://www.amazon.com/Modern-Cosmology-Scott-Dodelson/dp/0128159480)
2. This EFTofLSS chapter by Tobias Baldauf also nice: [here](https://academic.oup.com/book/44246/chapter-abstract/372565490?redirectedFrom=fulltext)
3. If you like lecture videos, the series of lectures by Marko Simonovic are very nice: [here](https://www.youtube.com/watch?v=UHCB6xJHa7U)
4. For some reason the website is down now, but the "Large-scale structure" slides by Emiliano Sefusatti, from the "Future Cosmology" school in Cargese, are very good: [here](https://www.cpt.univ-mrs.fr/~cosmo/EC2023/index.php)
5. Euclid forecast: https://arxiv.org/abs/1910.09273
